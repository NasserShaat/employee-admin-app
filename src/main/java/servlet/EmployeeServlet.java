package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmployeeDao;
import model.Employee;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/Employee")
/**
 * Servlet with doGet and doPost methods. When a GET method is called, this app
 * will show all employees from database.
 */
public class EmployeeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		EmployeeDao employeeDao = new EmployeeDao();
		List<Employee> employeeList = new ArrayList<Employee>();

		if (req.getParameter("employeeId") != null) {
			int employeeId = Integer.parseInt(req.getParameter("employeeId"));
			Employee withId = employeeDao.getWithId(employeeId);
			employeeList.add(withId);
		} else {
			employeeList = employeeDao.getAllEmployees();
		}
		req.setAttribute("employee", employeeList);
		req.getRequestDispatcher("src/main/webapp/index.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.sendRedirect(getServletContext().getContextPath() + "/");

	}

}
