package dao;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Employee;

/**
 * DAO class
 */
@SuppressWarnings("unchecked")
@Named("entity")
public class EmployeeDao {
	@PersistenceContext
	EntityManager em;

	/**
	 * List all employees
	 * 
	 * @return employees
	 */
	public List<Employee> getAllEmployees() {
		List<Employee> employees = em.createQuery("FROM Employee").getResultList();
		return employees;
	}

	/**
	 * Find employee by id
	 * 
	 * @param id
	 * @return employee
	 */
	public Employee getWithId(int id) {

		return em.find(Employee.class, id);
	}

	/**
	 * Find employee by name
	 * 
	 * @param name
	 * @return employee
	 */
	public Employee getWithName(String name) {

		return em.find(Employee.class, name);
	}

	/**
	 * Add employee to database
	 * 
	 * @param employee
	 */
	public void insert(Employee employee) {
		em.persist(employee);
	}

	/**
	 * Update the database with employees
	 * 
	 * @param employee
	 */
	public void update(Employee employee) {
		em.merge(employee);
	}

	/**
	 * Remove employee from database
	 * 
	 * @param employee
	 */
	public void remove(Employee employee) {
		em.remove(employee);
	}

}
